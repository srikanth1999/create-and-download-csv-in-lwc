  downloadCSVFile(fileName,exportFileData) {
    let rowEnd = '\n';
    let csvString = '';
    if (exportFileData.length != 0) {
      csvString += exportFileData[0];
      csvString += rowEnd;
    }
    for (let i = 1; i < exportFileData.length; i++) {
      csvString += exportFileData[i];
      csvString += rowEnd;
    }
    let downloadElement = document.createElement('a');
    downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
    downloadElement.target = '_self';
    downloadElement.download = fileName + '.csv';
    document.body.appendChild(downloadElement);
    downloadElement.click();
    const evt = new ShowToastEvent({
      title: "Success",
      message: 'File downloaded successfully.',
      variant: "success"
    });
    this.dispatchEvent(evt);
  }